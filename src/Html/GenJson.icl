implementation module Html.GenJson

import Text.GenJSON

import qualified Html

derive JSONEncode 'Html'.HtmlTag, 'Html'.HtmlAttr
derive JSONEncode 'Html'.SVGElt, 'Html'.SVGAttr, 'Html'.SVGAlign, 'Html'.SVGColor, 'Html'.SVGDefer,
	'Html'.SVGFillOpacity, 'Html'.SVGFuncIRI, 'Html'.SVGLengthAdjust,
	'Html'.SVGLengthUnit, 'Html'.SVGLineCap, 'Html'.SVGFillRule, 'Html'.SVGLineJoin, 'Html'.SVGMeetOrSlice,
	'Html'.SVGStrokeMiterLimit, 'Html'.SVGPaint, 'Html'.SVGStrokeDashArray, 'Html'.SVGStrokeDashOffset,
	'Html'.SVGStrokeWidth, 'Html'.SVGTransform, 'Html'.SVGZoomAndPan, 'Html'.SVGICCColor, 'Html'.SVGLength
derive JSONDecode 'Html'.HtmlTag, 'Html'.HtmlAttr
derive JSONDecode 'Html'.SVGElt, 'Html'.SVGAttr, 'Html'.SVGAlign, 'Html'.SVGColor, 'Html'.SVGDefer,
	'Html'.SVGFillOpacity, 'Html'.SVGFuncIRI, 'Html'.SVGLengthAdjust,
	'Html'.SVGLengthUnit, 'Html'.SVGLineCap, 'Html'.SVGFillRule, 'Html'.SVGLineJoin, 'Html'.SVGMeetOrSlice,
	'Html'.SVGStrokeMiterLimit, 'Html'.SVGPaint, 'Html'.SVGStrokeDashArray, 'Html'.SVGStrokeDashOffset,
	'Html'.SVGStrokeWidth, 'Html'.SVGTransform, 'Html'.SVGZoomAndPan, 'Html'.SVGICCColor, 'Html'.SVGLength
