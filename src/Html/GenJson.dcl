definition module Html.GenJson

from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from Html import qualified :: HtmlTag, :: HtmlAttr,
	:: SVGElt, :: SVGAttr, :: SVGAlign, :: SVGColor, :: SVGDefer, :: SVGFillOpacity, :: SVGFuncIRI, :: SVGLengthAdjust,
	:: SVGICCColor, :: SVGLengthUnit, :: SVGLineCap, :: SVGFillRule, :: SVGLineJoin, :: SVGMeetOrSlice,
	:: SVGStrokeMiterLimit, :: SVGPaint, :: SVGLength,
	:: SVGStrokeDashArray, :: SVGStrokeDashOffset, :: SVGStrokeWidth, :: SVGTransform, :: SVGZoomAndPan

derive JSONEncode 'Html'.HtmlTag, 'Html'.HtmlAttr
derive JSONEncode 'Html'.SVGElt, 'Html'.SVGAttr, 'Html'.SVGAlign, 'Html'.SVGColor, 'Html'.SVGDefer,
	'Html'.SVGFillOpacity, 'Html'.SVGFuncIRI, 'Html'.SVGLengthAdjust,
	'Html'.SVGLengthUnit, 'Html'.SVGLineCap, 'Html'.SVGFillRule, 'Html'.SVGLineJoin, 'Html'.SVGMeetOrSlice,
	'Html'.SVGStrokeMiterLimit, 'Html'.SVGPaint, 'Html'.SVGStrokeDashArray, 'Html'.SVGStrokeDashOffset,
	'Html'.SVGStrokeWidth, 'Html'.SVGTransform, 'Html'.SVGZoomAndPan, 'Html'.SVGICCColor, 'Html'.SVGLength
derive JSONDecode 'Html'.HtmlTag, 'Html'.HtmlAttr
derive JSONDecode 'Html'.SVGElt, 'Html'.SVGAttr, 'Html'.SVGAlign, 'Html'.SVGColor, 'Html'.SVGDefer,
	'Html'.SVGFillOpacity, 'Html'.SVGFuncIRI, 'Html'.SVGLengthAdjust,
	'Html'.SVGLengthUnit, 'Html'.SVGLineCap, 'Html'.SVGFillRule, 'Html'.SVGLineJoin, 'Html'.SVGMeetOrSlice,
	'Html'.SVGStrokeMiterLimit, 'Html'.SVGPaint, 'Html'.SVGStrokeDashArray, 'Html'.SVGStrokeDashOffset,
	'Html'.SVGStrokeWidth, 'Html'.SVGTransform, 'Html'.SVGZoomAndPan, 'Html'.SVGICCColor, 'Html'.SVGLength
